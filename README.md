# vue-user-table

Приложение реализованно на: JS c использованием Vue, также дополнительных библиотек -, axios, bulma, vue-router, vuex.

Приложение vue-user-table - выводит фильтруемый/сортируемый по полям список пользователей и позволяте посмотреть назначенные им задачи. Главная страница содержит список пользователей - https://jsonplaceholder.typicode.com/users полученный из этого сервиса.
На второй странице показана карточка пользователя, включая все его поля и выведены его задачи (https://jsonplaceholder.typicode.com/todos?userId=7).
Чекбокс показывает статус выполнения, без возможности изменения.
Список отсортирован по статусу, сверху идут невыполненные задачи.

Для запуска на локальной машине необходимо выполнить следующие команды.
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# UserTable
    Компонет выводящий список пользователей, селекты фильтрации по названию компании и выбора сортировки.
# Grid
    - Таблица,
    PROPS
        "data" массив данных.
        "columns" массив колонок {
                                title: 'String',
                                sort: 'Boolean',
                                width: 'String'('120px'),
                              }
        "filterKey" название выбранной компании
# UserProfile
- Выводит два таба и имя пользователя
# PersonTab
- Выводит информацию о пользователе, список всех его полей.
# TaskTab
- Выводит список всех задач пользователя
# UserNotFound
- Пользователь не найден.
# Select -
    PROPS
        label: {    Подпись
          type: String,
          default: '-- select a company --',
          required: false,
        },
        value: {    Текущее значение
          type: String,
          default: null,
          required: false,
        },
        options: {  Список опшинов
          type: Array,
          default: () => [],
          required: true,
        },
# Spinner
- спинер выводящийся во время загрузки данных.
