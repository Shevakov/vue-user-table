import axios from 'axios';

class usersApi {
  static getAllUsers() {
    return axios.get('https://jsonplaceholder.typicode.com/users')
      .then(res => res.data)
      .catch(err => err);
  }

  static getUsersByd(id) {
    return axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(res => res.data);
  }

  static getTaskByUsersId(userId) {
    return axios.get('https://jsonplaceholder.typicode.com/todos', { params: { userId } })
      .then(res => res.data)
      .catch(err => err);
  }
}

export default usersApi;
