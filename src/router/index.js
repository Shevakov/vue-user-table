import Vue from 'vue';
import Router from 'vue-router';
import UserTable from '@/components/UserTable';
import UserProfile from '@/components/UserProfile/UserProfile';
import UserNotFound from '@/components/UserProfile/UserNotFound';
import NotFound from '@/components/common/PageNotFound';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/users',
    },
    {
      path: '/users',
      name: 'Users',
      component: UserTable,
    },
    {
      path: '/user/:id',
      name: 'UserProfile',
      component: UserProfile,
    },
    {
      path: '/user/',
      name: 'UserNotFound',
      component: UserNotFound,
    }, {
      path: '/404',
      name: '404',
      component: NotFound,
    }, {
      path: '*',
      redirect: '/404',
    },
  ],
});
