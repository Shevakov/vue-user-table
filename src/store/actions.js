import usersApi from '../api/usersApi';

const actions = {
  fetchUsers({ commit }) {
    commit('loadingState', { isLoading: true });
    usersApi.getAllUsers()
      .then((data) => {
        commit('receivedUsers', data);
        commit('loadingState', { isLoading: false });
      })
      .catch(err => err);
  },
  fetchUserById({ commit }, { id }) {
    commit('loadingState', { isLoading: true });
    return usersApi.getUsersByd(id)
      .then((data) => {
        commit('singleUser', data);
        commit('loadingState', { isLoading: false });
      }, (err) => {
        commit('loadingState', { isLoading: false });
        return err;
      })
      .catch(err => err);
  },
  fetchTasksById({ commit }, { id }) {
    commit('loadingState', { isLoading: true });
    usersApi.getTaskByUsersId(id)
      .then((data) => {
        commit('tasks', data);
        commit('loadingState', { isLoading: false });
      })
      .catch(err => err);
  },
};

export default actions;
