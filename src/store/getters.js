const getters = {
  isLoading(state) {
    return state.isLoading;
  },
  allUsers(state) {
    return state.users;
  },
  singleUser(state) {
    return state.singleUser;
  },
  tasks(state) {
    return state.tasks && state.tasks.sort((a, b) => b - a).sort((x, y) => {
      if (x.completed === y.completed) {
        return 0;
      }
      return x.completed ? 1 : -1;
    });
  },
  allCompany(state) {
    return state.users.map(user => ({
      value: user.company.name,
      name: user.company.name,
    })).sort((a, b) => a.value.localeCompare(b.value));
  },
};

export default getters;
