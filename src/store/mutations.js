const mutations = {
  loadingState(state, { isLoading }) {
    state.isLoading = isLoading;
  },
  receivedUsers(state, users) {
    state.users = users;
  },
  singleUser(state, contact) {
    state.singleUser = contact;
  },
  tasks(state, tasks) {
    state.tasks = tasks;
  },
  setSortKey(state, key) {
    state.sortOrders = {
      ...state.sortOrders,
      [key]: state.sortOrders[key] * -1,
    };
    state.sortKey = key;
  },
  initSortOrders(state, columns) {
    columns.forEach((key) => {
      state.sortOrders[key.value] = state.sortOrders[key.value] || 1;
    });
  },
  setСurrentPage(state, page) {
    state.currentPage = page;
  },
  setCompany(state, page) {
    state.company = page;
  },
};

export default mutations;
