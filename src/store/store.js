import Vue from 'vue';
import Vuex from 'vuex';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    isLoading: false,
    singleUser: null,
    users: [],
    tasks: null,
    sortKey: 'name',
    sortOrders: {},
    currentPage: 1,
    company: '',
  },
  getters,
  mutations,
  actions,
});

export default store;
